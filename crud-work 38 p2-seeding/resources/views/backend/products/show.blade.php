<x-backend.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">Products</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Products</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Product Details
                                <a class="btn btn-sm btn-primary" href="{{ route('products.index')}}">Product List </a>
                            </div>
                            <div class="card-body">
                               <h3> Title: {{$product->title}}</h3>
                               <p> Description: {{$product->description}}</p>
                               <p> Price: {{ $product->price }} </p>
                               <img src="{{asset('storage/products/' . $product->image) }}"/>
                               <p> Created At: {{$product->created_at->diffForHumans() }} </p>
                               <p> Updated At: {{$product->updated_at->diffForHumans() }} </p>
                            </div>
                        </div>
                    </div>
</x-backend.layouts.master>