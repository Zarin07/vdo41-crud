<x-backend.layouts.master>
<div class="container-fluid px-4">
                        <h1 class="mt-4">Products</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">products</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Product Create
                                <a class="btn btn-sm btn-primary" href="{{ route('products.index')}}"> Product List</a>
                            </div>
                            <div class="card-body">

   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('products.store')}}" method="POST">
    @csrf
                

                <div class="mb-3">
                    <label for="title" class="form-label">Title</label>
                    <input name="title" type="text" class="form-control" id="title" value="{{old('title')}}">
                    
                    @error('title')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>


                <div class="mb-3">
                    <label for="price" class="form-label">Price</label>
                    <input name="price" type="number" class="form-control" id="price" value="{{old('price')}}" >
                    
                    @error('price')
                    <div  class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
  
                <button type="submit" class="btn btn-primary">Save</button>
</form>
                            </div>
                        </div>
                   
</x-backend.layouts.master>